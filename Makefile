build:
	go build .

test:
	go test -v -coverprofile=coverage.out  -timeout 30s -run .

coverage:
	go tool cover -html=coverage.out

ci_coverage:
	go tool cover -func=coverage.out
	
ci_coverage_check:
	make ci_coverage | grep "total:" |grep "100.0%"

pre_review: start test coverage ci_coverage_check
