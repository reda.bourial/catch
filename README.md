# Catch 
Catch is a simple way for inlining Golang recover mechanism.
The implementation is thread safe.
<h2 style="color:red"> Do not use it before reading the perfomance cost section</h2>

# Install :
    go get gitlab.com/reda.bourial/catch
# Usage
This functions illustrate the cases : 
```Go
    func panicWithObj(){
        panic(42)
    }
    func panicWithNil(){
        panic(nil)
    }

    func noPanic(){
        // does stuff but never panic
    }
    func functionThatPanics(string,string)(string,string){
        panic(42)
    }

    func functionThatSwitches(s1 string,s2 string) (string,string){
        // switching values (see Sanitize function)
        return s2,s1
    }
    
```
## Inlining to interface{}
``` Go
    // callback has to be type func()
    err := catch.Interface(callback)
    // err is type interface{}
```
|    callback      |            err    value         | err typed       |
|:----------------:|:-------------------------------:|:---------------:|
|   panicWithObj   |                42               |       int       |
|   panicWithNil   | "panic called with a nil error" |      error      |
|      noPanic     |               nil               |        -        |

## Inlining to error
returns the same values as the previous example
``` Go
    // callback has to be type func()
    err := catch.Error(callback)
    // err is type error
```
## Inlining with details
``` Go
    panicked, err := catch.Panic(callback)
    // err is type interface{}
    // panicked is type bool 
```
|    callback      |            err    value         | panicked value  |
|:----------------:|:-------------------------------:|:---------------:|
|   panicWithObj   |                42               |      true       |
|   panicWithNil   |               nil               |      true       |
|      noPanic     |               nil               |      false      |
## Sanitize function
```Go
    // callback can be any func type
    sanitizedFunction := catch.Sanitize(callback)
    retValues, err :=  sanitizedFunction("hello","world")
```
### NB :
**no compiler checks for arguments types and number of arguments.**
sanitizedFunction is typed func(...interface{}) ([]interface{},error).


|    callback            |            retValues            |            err value            |
|:----------------------:|:-------------------------------:|:-------------------------------:|
|   panicWithNil         |               nil               | "panic called with a nil error" |
|   functionThatPanics   |               nil               |              42                 |
|   functionThatSwitches |        ["world","hello"]        |             nil                 |

# Performance cost 
here is the output from go test -bench=. comparing panic/recover to catch.
```
BenchmarkWithPanic/pure_go-16           1000000000               0.0000027 ns/op
BenchmarkWithPanic/catch-16             1000000000               0.0000065 ns/op
BenchmarkWithoutPanic/pure_go-16        1000000000               0.0000004 ns/op
BenchmarkWithoutPanic/catch-16          1000000000               0.0000010 ns/op

```
## Bottom line
catch is at worst 2,5 times slower than pure go.
